<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table = 'personas';
    protected $fillable = ['nombre','apellido_paterno','apellido_materno','fecha_nacimiento','curp','rfc','telefono_oficina','telefono_celular','direccion_id'];

    public function direccion(){
        return $this->belongsTo(Direccion::class,'direccion_id');
    }

    public function add($data){
        return $this->create($data);
    }
    public function edit($data){
        return $this->fill($data)->save();
    }
    public function getWithId($id){
        return $this->findOrFail($id);
    }
    public function scopeGetAll($query,$status){
        return $query->getWithStatus($status)->orderWithDateDesc();
    }
    public function scopeGetWithStatus($query,$status){
        return $query->where('estatus',$status);
    }
    public function scopeOrderWithDateDesc($query){
        return $query->orderBy('created_at','desc');
    }
}
