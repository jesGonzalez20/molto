<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Modulo extends Model
{
    protected  $table = 'modulos';
    protected $fillable = ['modulo','descripcion','duracion','plantel_id','estatus'];

    public function add($data){
        return $this->create($data);
    }
    public function edit($data){
        return $this->fill($data)->save();
    }
    public function getWIthId($id){
        return $this->findOrFail($id);
    }
    public function getPluck(){
        return $this->pluck('nombre','id');
    }
    public function scopeGetAll($query,$status){
        return $query->getWithStatus($status)->orderByWithDateDesc();
    }
    public function scopeGetWithStatus($query,$status){
        return $query->where('estatus',$status);
    }
    public function scopeOrderByWithDateDesc($query){
        return $query->orderBy('created_at','desc');
    }
}
