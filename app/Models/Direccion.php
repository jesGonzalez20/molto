<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    protected $table = 'direcciones';
    protected $fillable = ['direccion','colonia','codigo_postal','ciudad','estado','pais','tipo'];

    public function add($direccion){
        return $this->create($direccion);
    }
    public function edit($direccion){
        return $this->fill($direccion)->save();
    }
    public function getDireccionWithId($idDireccion){
        return $this->findOrFail($idDireccion);
    }
}
