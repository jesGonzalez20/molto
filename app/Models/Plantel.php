<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plantel extends Model
{
    protected $table = 'planteles';
    protected $fillable = ['clave_sep','nombre','telefono_oficina','telefono_celular','direccion_id','representante_id','estatus'];

    public function direccion(){
        return $this->belongsTo(Direccion::class,'direccion_id');
    }
    public function representante(){
        return $this->belongsTo(Representante::class,'representante_id');
    }
    public function getPluck(){
        return $this->pluck('nombre','id');
    }
    public function add($data){
        return $this->create($data);
    }
    public function edit($data){
        return $this->fill($data)->save();
    }
    public function getWithId($idPlantel){
        return $this->findOrFail($idPlantel);
    }
    public function remove(){
        return $this->delete();
    }
    public function inactive(){
        return $this->fill(['estatus'=>"inactivo"])->save();
    }
    public function scopeGetAll($query,$status){
        return $query->getWithStatus($status)->orderByWithDateDesc();
    }
    public function scopeGetWithStatus($query,$status){
        return $query->where('estatus',$status);
    }
    public function scopeOrderByWithDateDesc($query){
        return $query->orderBy('created_at','desc');
    }
}
