<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    protected $table = 'estudiantes';
    protected $fillable = ['credencial','nombre_tutor','apellido_tutor','telefono_tutor','plantel_id','persona_id','email'];

    public function persona(){
        return $this->belongsTo(Persona::class,'persona_id');
    }
    public function plantel(){
        return $this->belongsTo(Plantel::class, 'plantel_id');
    }

    public function add($student){
        return $this->create($student);
    }
    public function edit($student){
        return $this->fill($student)->save();
    }
    public function getWithId($idStudent){
        return $this->findOrFail($idStudent);
    }
    public function scopeGetAll($query,$status){
        return $query->orderWithDateDesc();
    }
    public function scopeGetWithStatus($query,$status){
        return $query->where('estatus',$status);
    }
    public function scopeOrderWithDateDesc($query){
        return $query->orderBy('created_at','desc');
    }
}
