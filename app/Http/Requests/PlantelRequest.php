<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlantelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'clave_sep'=>'required',
            'telefono_oficina'=>'required|max:15',
            'telefono_celular'=>'required|max:15',
            'estatus'=>'required',
            'direccion'=>'required',
            'colonia'=>'required',
            'codigo_postal'=>'required|max:6',
            'ciudad'=>'required',
            'estado'=>'required',
            'pais'=>'required',
            'representante_id'=>'required'
        ];
    }
}
