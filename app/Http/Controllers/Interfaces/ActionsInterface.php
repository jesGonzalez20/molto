<?php


namespace App\Http\Controllers\Interfaces;


use http\Env\Request;

interface ActionsInterface
{
    public function getAll($status);
    public function getWIthId($idObject);
    public function add();
    public function update($object);
    public function delete($object,$action);
    public function buildStatus($status);
    public function setData($request);
}
