<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Repositories\ModuloRepository;
use App\Http\Controllers\Repositories\PlantelRepository;
use Illuminate\Http\Request;

class MateriasController extends Controller
{
    public function __construct(ModuloRepository $moduloRep)
    {
        $this->moduloRep = $moduloRep;
        $this->planteRep = new PlantelRepository();
    }

    public function index(Request $request)
    {
        $status = $this->moduloRep->buildStatus($request->status);
        $materias = $this->moduloRep->getAll($status);
        return view('admin.materias.index',compact('materias'));
    }

    public function store(Request $request)
    {
        try{
            $this->moduloRep->setData($request);
            $this->moduloRep->add();

            alert()->success('El Modulo fue agregado exitosamente','Registro Exitosa');
            return redirect('/materias');
        }catch(\Exception $e){
            dd($e);
        }
    }
    public function show($id)
    {
        //
    }
    public function update(Request $request, $idMateria)
    {
        try{
            $modulo = $this->moduloRep->getWIthId($idMateria);
            $this->moduloRep->setData($request);
            $this->moduloRep->update($modulo);

            alert()->success('El Modulo fue actualizado exitosamente','Actualización Exitosa');
            return redirect('/materias');
        }catch(\Exception $e){
            dd($e);
        }
    }
    public function destroy($id)
    {
        //
    }

    public function create()
    {
        $planteles = $this->planteRep->getPluck();
        return view('admin.materias.create',compact('planteles'));
    }
    public function edit($idModulo)
    {
        $planteles = $this->planteRep->getPluck();
        $modulo    = $this->moduloRep->getWIthId($idModulo);
        return view('admin.materias.edit',compact('planteles','modulo'));
    }
}
