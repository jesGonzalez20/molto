<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Repositories\AlumnoRepository;
use App\Http\Controllers\Repositories\DireccionRepository;
use App\Http\Controllers\Repositories\PersonaRepository;
use App\Http\Controllers\Repositories\PlantelRepository;
use App\Models\Direccion;
use App\Models\Persona;
use Illuminate\Http\Request;
use DB;

class AlumnoController extends Controller
{

    public function __construct(AlumnoRepository $alumnoRepository)
    {
        $this->alumnoRep    = $alumnoRepository;
        $this->plantelesRep = new PlantelRepository();
        $this->direccionRep = new DireccionRepository();
        $this->personaRep   = new PersonaRepository();
    }

    public function index(Request $request)
    {
        $status = $this->alumnoRep->buildStatus($request->estatus);
        $alumnos = $this->alumnoRep->getAll($status);
        $status = 0;
        return view('admin.alumnos.index',compact('alumnos'));
    }
    public function store(Request $request)
    {
        $requestTemp = new Request();
        try{
            DB::beginTransaction();

            $this->direccionRep->setData($request);
            $direccion = $this->direccionRep->add();

            $requestTemp = $this->personaRep->updateRequest($request,$direccion);
            $this->personaRep->setData($requestTemp);
            $persona = $this->personaRep->add();

            $requestTemp = $this->alumnoRep->updateRequest($request,$persona);
            $this->alumnoRep->setData($requestTemp);
            $this->alumnoRep->add();

            DB::commit();

        }catch(\Exception $e){
            DB::rollback();
            dd($e);
        }
    }
    public function show($id)
    {
        //
    }
    public function update(Request $request, $idAlumno)
    {
        $requestTemp = new Request();
        try{
            DB::beginTransaction();

            $alumno = $this->alumnoRep->getWIthId($idAlumno);

            $direccion = $this->direccionRep->getWIthId($alumno->persona->direccion_id);
            $this->direccionRep->setData($request);
            $this->direccionRep->update($direccion);

            $persona = $this->personaRep->getWIthId($alumno->persona_id);

            $this->personaRep->setData($request);
            $this->personaRep->update($persona);

            $this->alumnoRep->setData($request);
            $this->alumnoRep->update($alumno);

            DB::commit();

            alert()->success('El alumno fue actualizado exitosamente','Actualizacion Exitosa');
            return redirect('/alumnos');

        }catch(\Exception $e){
            DB::rollback();
            dd($e);
        }
    }
    public function destroy($id)
    {
        //
    }
    public function create(){
        $planteles = $this->plantelesRep->getPluck();
        $direccion = new Direccion();
        $persona = new Persona();
        $alumno  = $this->alumnoRep->alumno;
        return view('admin.alumnos.create',compact('planteles','direccion','persona','alumno'));
    }
    public function edit($idPlantel){
        $alumno = $this->alumnoRep->getWIthId($idPlantel);
        $planteles = $this->plantelesRep->getPluck();
        return view('admin.alumnos.edit',compact('alumno','planteles'));
    }
}
