<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Repositories\DireccionRepository;
use App\Http\Controllers\Repositories\PlantelRepository;
use App\Http\Requests\PlantelRequest;
use App\Http\Resources\Planteles\PlantelCollection;
use App\Http\Resources\Planteles\PlantelResource;
use App\Models\Plantel;
use App\Models\Representante;
use Illuminate\Http\Request;
use DB;
class PlantelController extends Controller
{

    public function __construct()
    {
        $this->plantelRep   = new PlantelRepository();
        $this->direccionRep = new DireccionRepository();
    }
    public function getPlantelWithId($idObject){
        return $this->plantelRep->getWIthId($idObject);
    }
    public function getDireccionWithId($idObject){
        return $this->direccionRep->getWIthId($idObject);
    }
    public function index(Request $request)
    {
        $status = $this->plantelRep->buildStatus($request->estatus);
        $planteles = $this->plantelRep->getAll($status);
        return view('admin.planteles.index',compact('planteles'));
    }
    public function store(PlantelRequest $request)
    {
        try{
            DB::beginTransaction();

            $this->direccionRep->setData($request);
            $direccion = $this->direccionRep->add();

            $request = $this->plantelRep->updateRequest($request,$direccion);
            $this->plantelRep->setData($request);
            $this->plantelRep->add();

            DB::commit();

            alert()->success('El plantel fue agregado exitosamente','Registro Exitoso');
            return redirect('/planteles');

        }catch (\Exception $e){
            DB::rollBack();
            return $e->getMessage();
        }
    }
    public function show($idPlantel)
    {

    }
    public function update(PlantelRequest $request, $idPlantel)
    {
        try{
            DB::beginTransaction();

            $plantel   = $this->getPlantelWithId($idPlantel);
            $this->plantelRep->setData($request);
            $this->plantelRep->update($plantel);

            $direccionRep = new DireccionRepository();
            $direccion    = $direccionRep->getWIthId($plantel->direccion_id);
            $direccionRep->setData($request);
            $direccionRep->update($direccion);

            DB::commit();

            alert()->success('El plantel fue actualizado exitosamente','Actualizacion Exitosa');
            return redirect('/planteles');

        }catch (\Exception $e){
            DB::rollBack();
            return $e;
        }
    }
    public function create(){
        $representantes = Representante::pluck('nombre','id');
        $plantel = $this->plantelRep->plantel;
        $direccion = $this->direccionRep->direccion;
        return view('admin.planteles.create',compact('representantes','plantel','direccion'));
    }
    public function edit($idPlantel){
        $plantel = $this->plantelRep->getWIthId($idPlantel);
        $representantes = Representante::pluck('nombre','id');
        return view('admin.planteles.edit',compact('representantes','plantel'));
    }
}
