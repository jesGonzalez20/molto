<?php


namespace App\Http\Controllers\Repositories;


use App\Http\Controllers\Interfaces\ActionsInterface;
use App\Models\Direccion;

class DireccionRepository implements ActionsInterface
{
    public function __construct()
    {
        $this->direccion = new Direccion();
        $this->attr = ['direccion','colonia','codigo_postal','ciudad','estado','pais','tipo'];
        $this->data = null;
    }
    public function getAll($status)
    {
        return null;
    }
    public function getWIthId($idObject)
    {
        return $this->direccion->getDireccionWithId($idObject);
    }
    public function add()
    {
        return $this->direccion->add($this->data);
    }
    public function update($direccion)
    {
        return $direccion->edit($this->data);
    }
    public function delete($object, $action)
    {
        return new HelperDelete($object,$action);
    }
    public function buildStatus($status)
    {
        return $status == null ? 'activo' : $status;
    }
    public function setData($request)
    {
        $this->data = $request->only($this->attr);
    }
}
