<?php


namespace App\Http\Controllers\Repositories;


class HelperDelete
{
    public function __construct($action,$object)
    {
        $this->action = $action == null ? 'inactive' : $action;
        $this->object = $object;
        $this->options();
    }
    private function options(){
        switch ($this->action){
            case 'delete':
                return $this->destroy();
                break;
            case 'inactive':
                return $this->inactive();
                break;
        }
    }
    private function destroy(){
        return $this->object()->remove();
    }
    private function inactive(){
        return $this->object->inactive();
    }
}
