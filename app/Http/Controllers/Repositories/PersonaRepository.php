<?php


namespace App\Http\Controllers\Repositories;


use App\Http\Controllers\Interfaces\ActionsInterface;
use App\Models\Persona;

class PersonaRepository implements ActionsInterface
{

    public function __construct()
    {
        $this->persona = new Persona();
        $this->attr =  ['nombre','apellido_paterno','apellido_materno','fecha_nacimiento','curp','rfc','telefono_oficina','telefono_celular','direccion_id'];
        $this->data = null;
    }

    public function getAll($status)
    {
        return null;
    }

    public function getWIthId($idObject)
    {
        return $this->persona->getWithId($idObject);
    }

    public function add()
    {
        return $this->persona->add($this->data);
    }

    public function update($object)
    {
        return $object->edit($this->data);
    }

    public function delete($object, $action)
    {
        return null;
    }

    public function buildStatus($status)
    {
        return null;
    }

    public function setData($request)
    {
       $this->data = $request->only($this->attr);
    }
    public function updateRequest($request,$direccion){
        $request['direccion_id'] = $direccion->id;
        return $request;
    }
}
