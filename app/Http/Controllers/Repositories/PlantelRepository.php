<?php


namespace App\Http\Controllers\Repositories;


use App\Http\Controllers\Interfaces\ActionsInterface;
use App\Models\Plantel;

class PlantelRepository implements ActionsInterface
{
    public function __construct()
    {
        $this->plantel = new Plantel();
        $this->attr = ['clave_sep','nombre','telefono_oficina','telefono_celular','direccion_id','representante_id'];
        $this->data = null;
    }
    public function getPluck(){
        return $this->plantel->getPluck();
    }
    public function getAll($status)
    {
        return $this->plantel->getAll($status)->get();
    }
    public function getWIthId($idPlantel)
    {
        return $this->plantel->getWithId($idPlantel);
    }
    public function add()
    {
        return $this->plantel->add($this->data);
    }
    public function update($plantel)
    {
        return $plantel->edit($this->data);
    }
    public function delete($plantel,$action)
    {
        return new HelperDelete($plantel,$action);
    }
    public function buildStatus($status)
    {
        return $status == null ? 'activo' : $status;
    }
    public function setData($request)
    {
        $this->data = $request->only($this->attr);
    }
    public function updateRequest($request,$direccion){
        $request['direccion_id'] = $direccion->id;
        return $request;
    }

}
