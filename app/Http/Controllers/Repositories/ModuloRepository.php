<?php


namespace App\Http\Controllers\Repositories;


use App\Http\Controllers\Interfaces\ActionsInterface;
use App\Models\Modulo;

class ModuloRepository implements ActionsInterface
{
    public function __construct()
    {
        $this->modulo = new Modulo();
        $this->attr = ['modulo','descripcion','duracion','plantel_id','estatus'];
        $this->data = null;
    }
    public function getAll($status)
    {
        return $this->modulo->getAll($status)->get();
    }
    public function getWIthId($idObject)
    {
        return $this->modulo->getWIthId($idObject);
    }
    public function add()
    {
        return $this->modulo->add($this->data);
    }
    public function update($object)
    {
       return $object->edit($this->data);
    }
    public function delete($object, $action)
    {
        return null;
    }
    public function buildStatus($status)
    {
        return $status == null ? 'activo' : $status;
    }
    public function setData($request)
    {
        $this->data = $request->only($this->attr);
    }
}
