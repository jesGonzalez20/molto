<?php


namespace App\Http\Controllers\Repositories;


use App\Http\Controllers\Interfaces\ActionsInterface;
use App\Models\Estudiante;

class AlumnoRepository implements ActionsInterface
{
    public function __construct()
    {
        $this->alumno = new Estudiante();
        $this->attr   = ['credencial','nombre_tutor','apellido_tutor','telefono_tutor','plantel_id','persona_id','email'];
        $this->data   = null;
    }
    public function getAll($status)
    {
        return $this->alumno->getAll($status)->get();
    }
    public function getWIthId($idObject)
    {
        return $this->alumno->getWithId($idObject);
    }
    public function add()
    {
       return $this->alumno->add($this->data);
    }
    public function update($student)
    {
        return $student->edit($this->data);
    }
    public function delete($object, $action)
    {
        return new HelperDelete($object,$action);
    }
    public function buildStatus($status)
    {
        return $status == null ? 'activo' : $status;
    }
    public function setData($request)
    {
        $this->data = $request->only($this->attr);
    }
    public function updateRequest($request,$persona){
        $request['persona_id'] = $persona->id;
        return $request;
    }
}
