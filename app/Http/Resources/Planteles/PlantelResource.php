<?php

namespace App\Http\Resources\Planteles;

use Illuminate\Http\Resources\Json\JsonResource;

class PlantelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
