
<div class="col-sm-6 col-md-6">
    <div class="form-group">
        {!! Form::label('email','Correo electronico : ',['class'=>'form-label']) !!}
        {!! Form::email('email','',['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo Correo electronico  es requerido
        </div>
        @error('email')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>
<div class="col-sm-6 col-md-6">
    <div class="form-group">
        {!! Form::label('password','Contraseña : ',['class'=>'form-label']) !!}
        {!! Form::password('password',['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo Contraseña  es requerido
        </div>
        @error('password')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>
<div class="col-sm-6 col-md-6">
    <div class="form-group">
        {!! Form::label('puesto_id','Puesto: ',['class'=>'form-label']) !!}
        {!! Form::select('puesto_id',['activo'=>'Activo','inactivo'=>'Inactivo'],'',['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo Puesto es requerido
        </div>
        @error('puesto_id')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>