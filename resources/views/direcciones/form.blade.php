<div class="col-md-8">
    <div class="form-group">
        {!! Form::label('direccion','Direccion: ',['class'=>'form-label']) !!}
        {!! Form::text('direccion',$direccion->direccion,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo dirección es requerido
        </div>
    </div>
</div>
<div class="col-sm-6 col-md-4">
    <div class="form-group">
        {!! Form::label('colonia','Colonia: ',['class'=>'form-label']) !!}
        {!! Form::text('colonia',$direccion->colonia,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo colonia es requerido
        </div>
    </div>
</div>
<div class="col-sm-6 col-md-4">
    <div class="form-group">
        {!! Form::label('codigo_postal','Codigo Postal: ',['class'=>'form-label']) !!}
        {!! Form::text('codigo_postal',$direccion->codigo_postal,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo codigo postal es requerido
        </div>
    </div>
</div>
<div class="col-sm-6 col-md-4">
    <div class="form-group">
        {!! Form::label('ciudad','Ciudad: ',['class'=>'form-label']) !!}
        {!! Form::text('ciudad',$direccion->ciudad,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo ciudad es requerido
        </div>
    </div>
</div>
<div class="col-sm-6 col-md-4">
    <div class="form-group">
        {!! Form::label('estado','Estado: ',['class'=>'form-label']) !!}
        {!! Form::text('estado',$direccion->estado,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo estado es requerido
        </div>
    </div>
</div>
<div class="col-sm-6 col-md-6">
    <div class="form-group">
        {!! Form::label('pais','Pais: ',['class'=>'form-label']) !!}
        {!! Form::text('pais',$direccion->pais,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo pais es requerido
        </div>
    </div>
</div>
