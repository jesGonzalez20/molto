<div class="col-sm-6 col-md-6">
    <div class="form-group">
        {!! Form::label('nombre','Nombre: ',['class'=>'form-label']) !!}
        {!! Form::text('nombre',$persona->nombre,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo nombre alumno es requerido
        </div>
    </div>
</div>
<div class="col-sm-3 col-md-3">
    <div class="form-group">
        {!! Form::label('apellido_paterno','Apellido paterno: ',['class'=>'form-label']) !!}
        {!! Form::text('apellido_paterno',$persona->apellido_paterno,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo apellido paterno es requerido
        </div>
    </div>
</div>
<div class="col-sm-3 col-md-3">
    <div class="form-group">
        {!! Form::label('apellido_materno','Apellido materno: ',['class'=>'form-label']) !!}
        {!! Form::text('apellido_materno',$persona->apellido_materno,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo apellido materno es requerido
        </div>
    </div>
</div>
<div class="col-sm-4 col-md-4">
    <div class="form-group">
        {!! Form::label('fecha_nacimiento','Fecha de nacimiento: ',['class'=>'form-label']) !!}
        {!! Form::date('fecha_nacimiento',$persona->fecha_nacimiento,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo fecha de nacimiento es requerido
        </div>
    </div>
</div>
<div class="col-sm-4 col-md-4">
    <div class="form-group">
        {!! Form::label('curp','CURP: ',['class'=>'form-label']) !!}
        {!! Form::text('curp',$persona->curp,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo curp es requerido
        </div>
    </div>
</div>
<div class="col-sm-4 col-md-4">
    <div class="form-group">
        {!! Form::label('rfc','RFC: ',['class'=>'form-label']) !!}
        {!! Form::text('rfc',$persona->rfc,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo curp es requerido
        </div>
    </div>
</div>

<div class="col-sm-6 col-md-6">
    <div class="form-group">
        {!! Form::label('telefono_oficina','Telefono Oficina: ',['class'=>'form-label']) !!}
        {!! Form::text('telefono_oficina',$persona->telefono_oficina,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo telefono oficina es requerido
        </div>
    </div>
</div>
<div class="col-sm-6 col-md-6">
    <div class="form-group">
        {!! Form::label('telefono_celular','Telefono Celular: ',['class'=>'form-label']) !!}
        {!! Form::text('telefono_celular',$persona->telefono_celular,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo telefono oficina es requerido
        </div>
    </div>
</div>
