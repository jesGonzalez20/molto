<table class="display table table-striped table-bordered" id="zero_configuration_table" style="width:100%">
    <thead>
    <tr>
        <th class="text-center">Plantel</th>
        <th class="text-center">Horario</th>
        <th class="text-center">Turno</th>
        <th class="text-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    {{--@foreach($materias as $materia)
        <tr>
            <td class="text-center">{{$materia->modulo}}</td>
            <td class="text-center">{{$materia->duracion}}</td>
            <td class="text-center">{{$materia->estatus}}</td>
            <td class="text-center">
                <a href="{{asset('materias/'.$materia->id.'/edit')}}" class="btn btn-primary btn-sm">Editar</a>
            </td>
        </tr>
    @--}}
    </tbody>
    <tfoot>
    <tr>
        <th class="text-center">Plantel</th>
        <th class="text-center">Horario</th>
        <th class="text-center">Turno</th>
        <th class="text-center">Acciones</th>
    </tr>
    </tfoot>
</table>