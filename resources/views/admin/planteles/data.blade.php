<table class="display table table-striped table-bordered" id="zero_configuration_table" style="width:100%">
    <thead>
    <tr>
        <th class="text-center">CVE SEP</th>
        <th class="text-center">Nombre</th>
        <th class="text-center">Representante</th>
        <th class="text-center">Telefono</th>
        <th class="text-center">Estatus</th>
        <th class="text-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach($planteles as $plantel)
        <tr>
            <td class="text-center">{{$plantel->clave_sep}}</td>
            <td class="text-center">{{$plantel->nombre}}</td>
            <td class="text-center">{{$plantel->representante->nombre}} {{$plantel->representante->apellidos}}</td>
            <td class="text-center">{{$plantel->telefono_oficina}}</td>
            <td class="text-center">{{$plantel->estatus}}</td>
            <td class="text-center">
                <a href="{{asset('planteles/'.$plantel->id.'/edit')}}" class="btn btn-info btn-sm text-uppercase">Editar</a>
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th class="text-center">CVE SEP</th>
        <th class="text-center">Nombre</th>
        <th class="text-center">Representante</th>
        <th class="text-center">Telefono</th>
        <th class="text-center">Estatus</th>
        <th class="text-center">Acciones</th>
    </tr>
    </tfoot>
</table>
