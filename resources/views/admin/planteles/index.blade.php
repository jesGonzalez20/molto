@extends('layouts.admin')
@section('content')
    <div class="breadcrumb">
        <h1>Planteles</h1>
        <ul>
            <li><a href="/">Dashboard</a></li>
            <li>Planteles</li>
        </ul>
    </div>
    <div class="separator-breadcrumb border-top"></div>
    <div class="row mb-4">
        <div class="col-md-12 mb-4">
            <div class="card text-left">
                <div class="card-body">
                    <h4 class="card-title mb-3 text-uppercase">Listado de planteles</h4>
                    <div class="table-responsive">
                        @include('admin.planteles.data')
                    </div>
                </div>
            </div>
        </div>
        <!-- end of col-->
    </div>
@stop
