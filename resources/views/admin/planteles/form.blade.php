<div class="col-sm-6 col-md-6">
    <div class="form-group">
        {!! Form::label('estatus','Estatus: ',['class'=>'form-label']) !!}
        {!! Form::select('estatus',['activo'=>'Activo','inactivo'=>'Inactivo'],$plantel->estatus,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo estatus es requerido
        </div>
        @error('estatus')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>
<div class="col-sm-6 col-md-6">
    <div class="form-group">
        {!! Form::label('clave_sep','Clave Sep: ',['class'=>'form-label']) !!}
        {!! Form::text('clave_sep',$plantel->clave_sep,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo clave sep es requerido
        </div>
        @error('clave_sep')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>
<div class="col-sm-12 col-md-12">
    <div class="form-group">
        {!! Form::label('nombre','Nombre: ',['class'=>'form-label']) !!}
        {!! Form::text('nombre',$plantel->nombre,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo nombre plantel es requerido
        </div>
        @error('nombre')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>

<div class="col-sm-6 col-md-6">
    <div class="form-group">
        {!! Form::label('telefono_oficina','Telefono Oficina: ',['class'=>'form-label']) !!}
        {!! Form::text('telefono_oficina',$plantel->telefono_oficina,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo telefono oficina es requerido
        </div>
        @error('telefono_oficina')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>
<div class="col-sm-6 col-md-6">
    <div class="form-group">
        {!! Form::label('telefono_celular','Telefono Celular: ',['class'=>'form-label']) !!}
        {!! Form::text('telefono_celular',$plantel->telefono_celular,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo telefono celular es requerido
        </div>
        @error('telefono_celular')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>
<div class="col-sm-12 col-md-12">
    <div class="form-group">
        {!! Form::label('representante_id','Representante: ',['class'=>'form-label']) !!}
        {!! Form::select('representante_id',$representantes,$plantel->representante,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo representante es requerido
        </div>
        @error('representante_id')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>


