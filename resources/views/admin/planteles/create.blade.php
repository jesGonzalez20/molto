@extends('layouts.admin')
@section('content')
    <div class="my-3 my-md-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <form action="{{asset('planteles')}}" class="needs-validation" novalidate="novalidate" method="POST">
                        @csrf
                        <div class="card">
                            <div class="card-header text-uppercase">Datos del plantel</div>
                            <div class="card-body">
                                <div class="card-body">
                                    <h3 class="card-title">Datos del plantel</h3>
                                    <div class="row">
                                        @include('admin.planteles.form',['plantel'=>$plantel])
                                        @include('direcciones.form',['direccion'=>$direccion])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button type="submit" class="btn btn-primary  m-1" data-style="expand-left"><span class="ladda-label"></span> Agregar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
