<div class="col-sm-6 col-md-6">
    <div class="form-group">
        {!! Form::label('credencial','Credencial: ',['class'=>'form-label']) !!}
        {!! Form::text('credencial',$alumno->credencial,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo Credencial  es requerido
        </div>
        @error('credencial')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>

<div class="col-sm-6 col-md-6">
    <div class="form-group">
        {!! Form::label('email','Email: ',['class'=>'form-label']) !!}
        {!! Form::text('email',$alumno->email,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo Email  es requerido
        </div>
        @error('email')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>

<div class="col-sm-12 col-md-6">
    <div class="form-group">
        {!! Form::label('plantel_id','Plantel: ',['class'=>'form-label']) !!}
        {!! Form::select('plantel_id',$planteles,$alumno->plantel_id,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo plantel  es requerido
        </div>
        @error('plantel_id')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>

<div class="col-sm-4 col-md-4">
    <div class="form-group">
        {!! Form::label('nombre_tutor','Nombre del tutor: ',['class'=>'form-label']) !!}
        {!! Form::text('nombre_tutor',$alumno->nombre_tutor,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo nombre tutor  es requerido
        </div>
        @error('nombre_tutor')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>

<div class="col-sm-4 col-md-4">
    <div class="form-group">
        {!! Form::label('apellido_tutor','Apellidos del tutor: ',['class'=>'form-label']) !!}
        {!! Form::text('apellido_tutor',$alumno->apellido_tutor,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo Apellido tutor  es requerido
        </div>
        @error('apellido_tutor')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>

<div class="col-sm-6 col-md-4">
    <div class="form-group">
        {!! Form::label('telefono_tutor','Telefono del tutor: ',['class'=>'form-label']) !!}
        {!! Form::text('telefono_tutor',$alumno->telefono_tutor,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo Telefono tutor  es requerido
        </div>
        @error('apellido_tutor')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>
