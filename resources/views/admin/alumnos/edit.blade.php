@extends('layouts.admin')
@section('content')
    <div class="my-3 my-md-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <form action="{{asset('alumnos/'.$alumno->id)}}" class="needs-validation" novalidate="novalidate" method="POST">
                        @csrf
                        {{method_field('put')}}
                        <div class="card">
                            <div class="card-header text-uppercase">Actualizar datos del plantel</div>
                            <div class="card-body">
                                <div class="card-body">
                                    <h3 class="card-title">Datos del plantel</h3>
                                    <div class="row">
                                        @include('personas.form',['persona'=>$alumno->persona])
                                        @include('direcciones.form',['direccion'=>$alumno->persona->direccion])
                                        @include('admin.alumnos.form',['alumno'=>$alumno])
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button type="submit" class="btn btn-primary  m-1" data-style="expand-left"><span class="ladda-label"></span> Actualizar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
