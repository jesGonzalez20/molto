<table class="display table table-striped table-bordered" id="zero_configuration_table" style="width:100%">
    <thead>
    <tr>
        <th class="text-center">Nombre</th>
        <th class="text-center">Apellidos</th>
        <th class="text-center">Plantel</th>
        <th class="text-center">Email</th>
        <th class="text-center">Telefono 1</th>
        <th class="text-center">Telefono 2</th>
        <th class="text-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach($alumnos as $alumno)
        <tr>
            <td class="text-center">{{$alumno->persona->nombre}}</td>
            <td class="text-center">{{$alumno->persona->apellido_paterno}} {{$alumno->persona->apellido_materno}}</td>
            <td class="text-center">{{$alumno->plantel->nombre}}</td>
            <td class="text-center">{{$alumno->email}}</td>
            <td class="text-center">{{$alumno->persona->telefono_celular}}</td>
            <td class="text-center">{{$alumno->persona->telefono_oficina}}</td>
            <td class="text-center">
                <a href="{{asset('alumnos/'.$alumno->id.'/edit')}}" class="btn btn-primary btn-sm">Editar</a>
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th class="text-center">Nombre</th>
        <th class="text-center">Apellidos</th>
        <th class="text-center">Plantel</th>
        <th class="text-center">Email</th>
        <th class="text-center">Telefono 1</th>
        <th class="text-center">Telefono 2</th>
        <th class="text-center">Acciones</th>
    </tr>
    </tfoot>
</table>
