@extends('layouts.admin')
@section('content')
    <div class="my-3 my-md-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <form action="{{asset('materias/'.$modulo->id)}}" class="needs-validation" novalidate="novalidate" method="POST">
                        @csrf
                        {{method_field('put')}}
                        <div class="card">
                            <div class="card-header text-uppercase">Actualizar modulo</div>
                            <div class="card-body">
                                <div class="card-body">
                                    <div class="row">
                                        @include('admin.materias.form')
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button type="submit" class="btn btn-primary  m-1" data-style="expand-left"><span class="ladda-label"></span> Actualizar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
