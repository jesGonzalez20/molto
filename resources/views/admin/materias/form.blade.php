<div class="col-sm-12 col-md-6">
    <div class="form-group">
        {!! Form::label('plantel_id','Plantel: ',['class'=>'form-label']) !!}
        {!! Form::select('plantel_id',$planteles,$modulo->plantel_id,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo Plantel es requerido
        </div>
        @error('plantel_id')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>
<div class="col-sm-12 col-md-6">
    <div class="form-group">
        {!! Form::label('estatus','Estatus: ',['class'=>'form-label']) !!}
        {!! Form::select('estatus',['activo'=>'Activo','inactivo'=>'Inactivo'],$modulo->estatus,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo estatus es requerido
        </div>
        @error('estatus')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>
<div class="col-sm-6 col-md-6">
    <div class="form-group">
        {!! Form::label('modulo','Nombre del modulo : ',['class'=>'form-label']) !!}
        {!! Form::text('modulo',$modulo->modulo,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo Modulo  es requerido
        </div>
        @error('modulo')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>
<div class="col-sm-6 col-md-6">
    <div class="form-group">
        {!! Form::label('duracion','Duracion : ',['class'=>'form-label']) !!}
        {!! Form::number('duracion',$modulo->duracion,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo Modulo  es requerido
        </div>
        @error('duracion')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>
<div class="col-sm-12 col-md-12">
    <div class="form-group">
        {!! Form::label('descripcion','Descripcion : ',['class'=>'form-label']) !!}
        {!! Form::textarea('descripcion',$modulo->descripcion,['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo Descripcion es requerido
        </div>
        @error('descripcion')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>
