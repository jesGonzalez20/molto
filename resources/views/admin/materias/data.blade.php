<table class="display table table-striped table-bordered" id="zero_configuration_table" style="width:100%">
    <thead>
    <tr>
        <th class="text-center">Módulo</th>
        <th class="text-center">Duración</th>
        <th class="text-center">Estatus</th>
        <th class="text-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach($materias as $materia)
        <tr>
            <td class="text-center">{{$materia->modulo}}</td>
            <td class="text-center">{{$materia->duracion}}</td>
            <td class="text-center">{{$materia->estatus}}</td>
            <td class="text-center">
                <a href="{{asset('materias/'.$materia->id.'/edit')}}" class="btn btn-primary btn-sm">Editar</a>
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <th class="text-center">Módulo</th>
        <th class="text-center">Duración</th>
        <th class="text-center">Estatus</th>
        <th class="text-center">Acciones</th>
    </tr>
    </tfoot>
</table>
