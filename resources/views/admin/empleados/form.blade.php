<div class="col-sm-12 col-md-12">
    <div class="form-group">
        {!! Form::label('plantel_id','Plantel: ',['class'=>'form-label']) !!}
        {!! Form::select('plantel_id',['activo'=>'Activo','inactivo'=>'Inactivo'],'',['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo estatus es requerido
        </div>
        @error('plantel_id')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>
<div class="col-sm-6 col-md-6">
    <div class="form-group">
        {!! Form::label('nss','NSS : ',['class'=>'form-label']) !!}
        {!! Form::text('nss','',['class'=>'form-control','id'=>'validationTooltip01','required'=>true]) !!}
        <div class="invalid-tooltip">
            El campo NSS  es requerido
        </div>
        @error('nss')
            <span class="invalid-tooltip" style="display: block !important;">{{$message}}</span>
        @enderror
    </div>
</div>