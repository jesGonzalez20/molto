<!DOCTYPE html>
<html lang="en" dir="">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Moltobella </title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet" />
    <link href="{{asset('dist-assets/css/themes/lite-purple.min.css')}}" rel="stylesheet" />
    <link href="{{asset('dist-assets/css/plugins/perfect-scrollbar.min.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('dist-assets/css/plugins/datatables.min.css')}}" />
    <link rel="stylesheet" href="{{asset('dist-assets/css/plugins/ladda-themeless.min.css')}}" />
</head>

<body class="text-left">
<div class="app-admin-wrap layout-sidebar-compact sidebar-dark-purple sidenav-open clearfix">
    <div class="side-content-wrap">
        <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
            <ul class="navigation-left">
                <li class="nav-item " data-item="dashboard">
                    <a class="nav-item-hold" href="#">
                        <i class="nav-icon i-Bar-Chart"></i>
                        <span class="nav-text">Dashboard</span>
                    </a>
                    <div class="triangle"></div>
                </li>
                <li class="nav-item active" data-item="uikits">
                    <a class="nav-item-hold" href="#">
                        <i class="nav-icon i-Home-2"></i>
                        <span class="nav-text">Planteles</span>
                    </a>
                    <div class="triangle"></div>
                </li>
                <li class="nav-item" data-item="extrakits">
                    <a class="nav-item-hold" href="#">
                        <i class="nav-icon i-Love-User"></i>
                        <span class="nav-text">Alumnos</span>
                    </a>
                    <div class="triangle"></div>
                </li>
                <li class="nav-item" data-item="apps">
                    <a class="nav-item-hold" href="#">
                        <i class="nav-icon i-Computer-Secure"></i>
                        <span class="nav-text">Modulos</span>
                    </a>
                    <div class="triangle"></div>
                </li>
                <li class="nav-item" data-item="forms">
                    <a class="nav-item-hold" href="#">
                        <i class="nav-icon i-File-Clipboard-File--Text"></i>
                        <span class="nav-text">Forms</span>
                    </a>
                    <div class="triangle"></div>
                </li>
                <li class="nav-item">
                    <a class="nav-item-hold" href="datatables.html">
                        <i class="nav-icon i-File-Horizontal-Text"></i>
                        <span class="nav-text">Datatables</span>
                    </a>
                    <div class="triangle"></div>
                </li>
                <li class="nav-item" data-item="sessions">
                    <a class="nav-item-hold" href="#">
                        <i class="nav-icon i-Administrator"></i>
                        <span class="nav-text">Sessions</span>
                    </a>
                    <div class="triangle"></div>
                </li>
                <li class="nav-item" data-item="others">
                    <a class="nav-item-hold" href="#">
                        <i class="nav-icon i-Double-Tap"></i>
                        <span class="nav-text">Others</span>
                    </a>
                    <div class="triangle"></div>
                </li>
                <li class="nav-item">
                    <a class="nav-item-hold" href="http://demos.ui-lib.com/gull-html-doc/" target="_blank">
                        <i class="nav-icon i-Safe-Box1"></i>
                        <span class="nav-text">Doc</span>
                    </a>
                    <div class="triangle"></div>
                </li>
            </ul>
        </div>
        <div class="sidebar-left-secondary rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
            <i class="sidebar-close i-Close" (click)="toggelSidebar()"></i>
            <header>
                <div class="logo">
                    <img src="../../dist-assets/images/logo-text.png" alt="">
                </div>
            </header>
            <!-- Submenu Dashboards -->
            <div class="submenu-area" data-parent="dashboard">
                <header>
                    <h6>Dashboards</h6>
                    <p>Lorem ipsum dolor sit.</p>
                </header>
                <ul class="childNav">
                    <li class="nav-item">
                        <a href="dashboard1.html">
                            <i class="nav-icon i-Clock-3"></i>
                            <span class="item-name">Version 1</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="dashboard2.html">
                            <i class="nav-icon i-Clock-4"></i>
                            <span class="item-name">Version 2</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="dashboard3.html">
                            <i class="nav-icon i-Over-Time"></i>
                            <span class="item-name">Version 3</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="dashboard4.html">
                            <i class="nav-icon i-Clock"></i>
                            <span class="item-name">Version 4</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="submenu-area" data-parent="forms">
                <header>
                    <h6>Forms</h6>
                    <p>Lorem ipsum dolor sit.</p>
                </header>
                <ul class="childNav">
                    <li class="nav-item">
                        <a href="form.basic.html">
                            <i class="nav-icon i-File-Clipboard-Text--Image"></i>
                            <span class="item-name">Basic Elements</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="form.layouts.html">
                            <i class="nav-icon i-Split-Vertical"></i>
                            <span class="item-name">Form Layouts</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="form.input.group.html">
                            <i class="nav-icon i-Receipt-4"></i>
                            <span class="item-name">Input Groups</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="form.validation.html">
                            <i class="nav-icon i-Close-Window"></i>
                            <span class="item-name">Form Validation</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="smart.wizard.html">
                            <i class="nav-icon i-Width-Window"></i>
                            <span class="item-name">Smart Wizard</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="tag.input.html">
                            <i class="nav-icon i-Tag-2"></i>
                            <span class="item-name">Tag Input</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="editor.html">
                            <i class="nav-icon i-Pen-2"></i>
                            <span class="item-name">Rich Editor</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="submenu-area" data-parent="apps">
                <header>
                    <h6>Academicos</h6>
                </header>
                <ul class="childNav">
                    <li class="nav-item">
                        <a href="/materias">
                            <i class="nav-icon i-Add-File"></i>
                            <span class="item-name">Listado de modulos</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{asset('materias/create')}}">
                            <i class="nav-icon i-Email"></i>
                            <span class="item-name">agregar nuevo</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="chat.html">
                            <i class="nav-icon i-Speach-Bubble-3"></i>
                            <span class="item-name">Chat</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="submenu-area" data-parent="extrakits">
                <header>
                    <h6>Alumnos</h6>
                </header>
                <ul class="childNav">
                    <li class="nav-item">
                        <a href="{{asset('alumnos/create')}}">
                            <i class="nav-icon i-Add-User"></i>
                            <span class="item-name">Agregar nuevo</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{asset('alumnos')}}">
                            <i class="nav-icon i-Split-Horizontal-2-Window"></i>
                            <span class="item-name">Historial</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="submenu-area" data-parent="uikits">
                <header>
                    <h6>Planteles</h6>
                    <p></p>
                </header>
                <ul class="childNav">
                    <li class="nav-item">
                        <a href="{{asset('planteles/create')}}">
                            <i class="nav-icon i-Add"></i>
                            <span class="item-name">Agregar nuevo</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{asset('planteles')}}">
                            <i class="nav-icon i-Split-Horizontal-2-Window"></i>
                            <span class="item-name">Historial</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="submenu-area" data-parent="sessions">
                <header>
                    <h6>Session Pages</h6>
                    <p>Lorem ipsum dolor sit.</p>
                </header>
                <ul class="childNav">
                    <li class="nav-item">
                        <a href="../sessions/signin.html">
                            <i class="nav-icon i-Checked-User"></i>
                            <span class="item-name">Sign in</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="../sessions/signup.html">
                            <i class="nav-icon i-Add-User"></i>
                            <span class="item-name">Sign up</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="../sessions/forgot.html">
                            <i class="nav-icon i-Find-User"></i>
                            <span class="item-name">Forgot</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="submenu-area" data-parent="others">
                <header>
                    <h6>Others</h6>
                    <p>Lorem ipsum dolor sit.</p>
                </header>
                <ul class="childNav" data-parent="">
                    <li class="nav-item">
                        <a href="../sessions/not-found.html">
                            <i class="nav-icon i-Error-404-Window"></i>
                            <span class="item-name">Not Found</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="user.profile.html">
                            <i class="nav-icon i-Male"></i>
                            <span class="item-name">User Profile</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="blank.html">
                            <i class="nav-icon i-File-Horizontal"></i>
                            <span class="item-name">Blank Page</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!--=============== Left side End ================-->
    <div class="main-content-wrap d-flex flex-column">
        <div class="main-header">
            <div class="logo">
                <img src="{{asset('dist-assets/images/logo.png')}}" alt="">
            </div>
            <div class="menu-toggle">
                <div></div>
                <div></div>
                <div></div>
            </div>
            <div class="d-flex align-items-center">
                <!-- Mega menu -->
                <div class="dropdown mega-menu d-none d-md-block">
                    <a href="#" class="btn text-muted dropdown-toggle mr-3" id="dropdownMegaMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Mega Menu</a>
                    <div class="dropdown-menu text-left" aria-labelledby="dropdownMenuButton">
                        <div class="row m-0">
                            <div class="col-md-4 p-4 bg-img">
                                <h2 class="title">Menu principa</h2>
                                <p>Podras encontrar las opciones mas utilizadas en el sistema.</p>
                            </div>
                            <div class="col-md-4 p-4">
                                <p class="text-primary text--cap border-bottom-primary d-inline-block">Opciones</p>
                                <div class="menu-icon-grid w-auto p-0">
                                    <a href="#"><i class="i-Shop-4"></i> Home</a>
                                    <a href="#"><i class="i-Library"></i> UI Kits</a>
                                    <a href="#"><i class="i-Drop"></i> Apps</a>
                                    <a href="#"><i class="i-File-Clipboard-File--Text"></i> Forms</a>
                                    <a href="#"><i class="i-Checked-User"></i> Sessions</a>
                                    <a href="#"><i class="i-Ambulance"></i> Support</a>
                                </div>
                            </div>
                            <div class="col-md-4 p-4">
                                <p class="text-primary text--cap border-bottom-primary d-inline-block">Components</p>
                                <ul class="links">
                                    <li><a href="accordion.html">Accordion</a></li>
                                    <li><a href="alerts.html">Alerts</a></li>
                                    <li><a href="buttons.html">Buttons</a></li>
                                    <li><a href="badges.html">Badges</a></li>
                                    <li><a href="carousel.html">Carousels</a></li>
                                    <li><a href="lists.html">Lists</a></li>
                                    <li><a href="popover.html">Popover</a></li>
                                    <li><a href="tables.html">Tables</a></li>
                                    <li><a href="datatables.html">Datatables</a></li>
                                    <li><a href="modals.html">Modals</a></li>
                                    <li><a href="nouislider.html">Sliders</a></li>
                                    <li><a href="tabs.html">Tabs</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- / Mega menu -->
                <div class="search-bar">
                    <input type="text" placeholder="Search">
                    <i class="search-icon text-muted i-Magnifi-Glass1"></i>
                </div>
            </div>
            <div style="margin: auto"></div>
            <div class="header-part-right">
                <div class="dropdown">
                    <div class="user col align-self-end">
                        <img src="{{asset('dist-assets/images/faces/1.jpg')}}" id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                            <div class="dropdown-header">
                                <i class="i-Lock-User mr-1"></i> Timothy Carlson
                            </div>
                            <a class="dropdown-item">Account settings</a>
                            <a class="dropdown-item">Billing history</a>
                            <a class="dropdown-item" href="signin.html">Sign out</a>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- ============ Body content start ============= -->
        <div class="main-content">
            <div id="app">
                @yield('content')
            </div>
            <!-- end of row-->
            <!-- end of main-content -->
            <!-- Footer Start -->
            <div class="flex-grow-1"></div>
            <div class="app-footer">
                <div class="row">
                    <div class="col-md-9">
                        <p><strong>Gull - Laravel + Bootstrap 4 admin template</strong></p>
                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Libero quis beatae officia saepe perferendis voluptatum minima eveniet voluptates dolorum, temporibus nisi maxime nesciunt totam repudiandae commodi sequi dolor quibusdam
                            <sunt></sunt>
                        </p>
                    </div>
                </div>
                <div class="footer-bottom border-top pt-3 d-flex flex-column flex-sm-row align-items-center">
                    <a class="btn btn-primary text-white btn-rounded" href="https://themeforest.net/item/gull-bootstrap-laravel-admin-dashboard-template/23101970" target="_blank">Buy Gull HTML</a>
                    <span class="flex-grow-1"></span>
                    <div class="d-flex align-items-center">
                        <img class="logo" src="../../dist-assets/images/logo.png" alt="">
                        <div>
                            <p class="m-0">&copy; 2018 Gull HTML</p>
                            <p class="m-0">All rights reserved</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- fotter end -->
        </div>
    </div>
</div><!-- ============ Search UI Start ============= -->
<div class="search-ui">
    <div class="search-header">
        <img src="../../dist-assets/images/logo.png" alt="" class="logo">
        <button class="search-close btn btn-icon bg-transparent float-right mt-2">
            <i class="i-Close-Window text-22 text-muted"></i>
        </button>
    </div>
    <input type="text" placeholder="Type here" class="search-input" autofocus>
    <div class="search-title">
        <span class="text-muted">Search results</span>
    </div>
    <div class="search-results list-horizontal">
        <div class="list-item col-md-12 p-0">
            <div class="card o-hidden flex-row mb-4 d-flex">
                <div class="list-thumb d-flex">
                    <!-- TUMBNAIL -->
                    <img src="../../dist-assets/images/products/headphone-1.jpg" alt="">
                </div>
                <div class="flex-grow-1 pl-2 d-flex">
                    <div class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                        <!-- OTHER DATA -->
                        <a href="" class="w-40 w-sm-100">
                            <div class="item-title">Headphone 1</div>
                        </a>
                        <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                        <p class="m-0 text-muted text-small w-15 w-sm-100">$300
                            <del class="text-secondary">$400</del>
                        </p>
                        <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                            <span class="badge badge-danger">Sale</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-item col-md-12 p-0">
            <div class="card o-hidden flex-row mb-4 d-flex">
                <div class="list-thumb d-flex">
                    <!-- TUMBNAIL -->
                    <img src="../../dist-assets/images/products/headphone-2.jpg" alt="">
                </div>
                <div class="flex-grow-1 pl-2 d-flex">
                    <div class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                        <!-- OTHER DATA -->
                        <a href="" class="w-40 w-sm-100">
                            <div class="item-title">Headphone 1</div>
                        </a>
                        <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                        <p class="m-0 text-muted text-small w-15 w-sm-100">$300
                            <del class="text-secondary">$400</del>
                        </p>
                        <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                            <span class="badge badge-primary">New</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-item col-md-12 p-0">
            <div class="card o-hidden flex-row mb-4 d-flex">
                <div class="list-thumb d-flex">
                    <!-- TUMBNAIL -->
                    <img src="../../dist-assets/images/products/headphone-3.jpg" alt="">
                </div>
                <div class="flex-grow-1 pl-2 d-flex">
                    <div class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                        <!-- OTHER DATA -->
                        <a href="" class="w-40 w-sm-100">
                            <div class="item-title">Headphone 1</div>
                        </a>
                        <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                        <p class="m-0 text-muted text-small w-15 w-sm-100">$300
                            <del class="text-secondary">$400</del>
                        </p>
                        <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                            <span class="badge badge-primary">New</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-item col-md-12 p-0">
            <div class="card o-hidden flex-row mb-4 d-flex">
                <div class="list-thumb d-flex">
                    <!-- TUMBNAIL -->
                    <img src="../../dist-assets/images/products/headphone-4.jpg" alt="">
                </div>
                <div class="flex-grow-1 pl-2 d-flex">
                    <div class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                        <!-- OTHER DATA -->
                        <a href="" class="w-40 w-sm-100">
                            <div class="item-title">Headphone 1</div>
                        </a>
                        <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                        <p class="m-0 text-muted text-small w-15 w-sm-100">$300
                            <del class="text-secondary">$400</del>
                        </p>
                        <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                            <span class="badge badge-primary">New</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- PAGINATION CONTROL -->
    <div class="col-md-12 mt-5 text-center">
        <nav aria-label="Page navigation example">
            <ul class="pagination d-inline-flex">
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- ============ Search UI End ============= -->
<script src="{{asset('dist-assets/js/plugins/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('dist-assets/js/plugins/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('dist-assets/js/plugins/perfect-scrollbar.min.js')}}"></script>
<script src="{{asset('dist-assets/js/scripts/script.min.js')}}"></script>
<script src="{{asset('dist-assets/js/scripts/sidebar.compact.script.min.js')}}"></script>
<script src="{{asset('dist-assets/js/plugins/datatables.min.js')}}"></script>
<script src="{{asset('dist-assets/js/scripts/datatables.script.min.js')}}"></script>
<script src="{{asset('dist-assets/js/scripts/customizer.script.min.js')}}"></script>
<script src="{{asset('dist-assets/js/scripts/form.validation.script.min.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="{{asset('dist-assets/js/plugins/spin.min.js')}}"></script>
<script src="{{asset('dist-assets/js/plugins/ladda.min.js')}}"></script>
<script src="{{asset('dist-assets/js/scripts/ladda.script.min.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>
@include('sweet::alert')
</body>

</html>
