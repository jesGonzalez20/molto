<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('planteles','PlantelController');
Route::resource('alumnos','AlumnoController');
Route::resource('materias','MateriasController');
Route::resource('horarios','HorarioController');
Route::resource('empleados','EmpleadoController');
