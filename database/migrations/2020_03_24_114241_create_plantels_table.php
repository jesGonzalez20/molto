<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlantelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planteles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('clave_sep');
            $table->string('nombre',32);
            $table->string('telefono_oficina',15);
            $table->string('telefono_celular',15);

            $table->enum('estatus',['activo','inactivo']);

            $table->integer('representante_id')->unsigned();
            $table->foreign('representante_id')->references('id')->on('representantes');
            $table->integer('direccion_id')->unsigned();
            $table->foreign('direccion_id')->references('id')->on('direcciones');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plantels');
    }
}
